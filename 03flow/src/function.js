/**
 * Created by qianzq.fnst on 2017/07/11.
 */
// @flow
function square(n: number): number {
    return n * n;
}

// square("2");